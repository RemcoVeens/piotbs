import time

import pychromecast


class Casting:
    def __init__(self) -> None:
        self.get_devices()

    def _selector(self, choice):
        return self.casts[choice]

    def get_devices(self):
        self.casts, self.browser = pychromecast.get_chromecasts()
        self.devices = {}
        for i, cc in enumerate(self.casts):
            self.devices[i] = cc.cast_info.friendly_name

    def select_device(self):
        choice = int(input("what device do you want to control: "))
        self.chosen_divice = self._selector(choice)
        self.chosen_divice.wait()

    def play(self, file):
        mc = self.chosen_divice.media_controller
        mc.play_media(
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
            "video/mp4",
        )
        mc.block_until_active()
        print(mc.status)
        mc.pause()
        time.sleep(5)
        mc.play()

    def main(self):
        if self.devices == {}:
            print("no divies found")
            return

        print(self.devices)
        self.select_device()
        print(self.chosen_divice)
        self.play("")


if __name__ == "__main__":
    caster = Casting()
    caster.main()
